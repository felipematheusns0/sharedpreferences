package br.ifsc.edu.blocodenotassharedpreferences;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.editText);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().toString().trim().equals("")) {
                    Toast.makeText(MainActivity.this, "NÃO HÁ TEXTO PARA SALVAR", Toast.LENGTH_LONG).show();
                } else {
                    editor.putString("notas", editText.getText().toString());
                    editor.commit();
                }
            }


        });
        sharedPreferences = getSharedPreferences("NotasBD", MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    @Override
    protected void onStart() {
        super.onStart();

        editText.setText(sharedPreferences.getString("notas", ""));
    }

    @Override
    protected void onPause() {
        super.onPause();

        editor.putString("notas", editText.getText().toString());
        editor.commit();
    }

    public void recuperar(View view) {
        if (sharedPreferences.contains("notas")) {
            editText.setText(sharedPreferences.getString("notas", ""));
        } else {
            Toast.makeText(MainActivity.this, "NÃO HÁ NOTAS PARA RECUPERAR :)", Toast.LENGTH_LONG).show();
        }
    }
}
